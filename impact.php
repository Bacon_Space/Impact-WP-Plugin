<?php
/**
 * Plugin Name: Impact Wordpress Plugin
 * Plugin URI: https://gitlab.com/Bacon_Space/Impact-WP-Plugin
 * Description: This is a plugin that provides information on how to watch Impact! Wrestling. Shortcode [impact]
 * Version: 1.0.0
 * Author: Bacon_Space
 * Author URI: https://twitter.com/Bacon_Space
 */


function impact() {
    $output = '<ul>';
    foreach ($suggestions as $suggestion) {
        $output .= '<li>' . $suggestion . '</li>';
    }
    $output .= '</ul>';
    
    // Additional information about where to watch Impact! Wrestling
    $output .= "<h4>Watch IMPACT! Wrestling</h4>";
    $output .= "<ul>";
    $output .= "<li>USA: Thursdays 8pm ET on <a href='https://www.axs.tv/'>AXS TV</a>, Thursdays 8:30pm ET on <a href='https://www.youtube.com/impactwrestling'>YouTube (IMPACT Insiders)</a>, Thursdays 10pm ET on <a href='https://impactplus.tv/' target='_blank'>IMPACT Plus</a></li>";
    $output .= "<li>Canada: Thursdays 8pm ET on <a href='https://www.fightnetwork.com/'>Fight Network</a> & AXS TV, Thursdays 8:30pm ET on <a href='https://www.youtube.com/impactwrestling'>YouTube (IMPACT Insiders)</a>, Thursdays 10pm ET on <a href='https://impactplus.tv/' target='_blank'>IMPACT Plus</a>, Saturdays 7pm ET on <a href='https://www.gametv.ca/'>GameTV</a></li>";
    $output .= "<li>UK & Ireland: Fridays 1:30am GMT on <a href='https://www.youtube.com/impactwrestling'>YouTube (IMPACT Insiders)</a>, Fridays 3am GMT on <a href='https://impactplus.tv/' target='_blank'>IMPACT Plus</a>, <a href='https://www.dazn.com/en-CA/sport/Sport:50dsk39gxuwwbkss8k2e24mca' target='_blank'>DAZN</a></li>";
    $output .= "</ul>";
    
    return $output;
}

add_shortcode('impact', 'impact');
